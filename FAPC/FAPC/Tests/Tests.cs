﻿using FAPC.PageObjects.Desktop;
using FAPC.PageObjects.Web;
using FAPC.PageObjects.Mobile;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using System.Net;
using FAPC.Controller;
using Newtonsoft.Json.Linq;

namespace FAPC.Tests
{
    public class Inicio
    {
        public string Cpf { get; set; }
    }

    [TestClass]
    [DeploymentItem(@"Deploy")]
    [DeploymentItem(@"Data")]
    public class Tests : TestBase
    {

        [TestMethod]
        public void Test1()
        {
            Test.Info("Cross Technology Test");

            //SetDriverType(DriverType.Mobile);

            //Logger = "Click Pesquisar";
            //HomePage.ClickPesquisar(30);

            SetDriverType(DriverType.Web);

            Driver.Url = Url;

            Logger = "Click Fale Conosco";
            IndexController.ClickFaleConosco();

            SetDriverType(DriverType.Desktop);

            Logger = "Click Num 2";
            CalcPage.ClickNum2();

            SetDriverType(DriverType.Service);
            Test2();
        }

        [TestMethod]
        public void Test2()
        {
            #region Params

            string iD;
            IRestResponse response;

            var model = new Inicio()
            {
                Cpf = "13089212099"
            };

            #endregion

            #region Scenario

            response = RunService("/api/Contatos/Inicio", model);
            iD = JObject.Parse(response.Content)["id"].ToString();

            Logger = $"Get Id: {iD}";
            Checkpoint(HttpStatusCode.OK == response.StatusCode, $"Response:\n{response.Content}", false);

            #endregion
        }
    }
}